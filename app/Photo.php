<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $fillable = [
        'properties_id', 'users_id', 'photo_name', 'is_feature', 'state'
    ];

    public function property(){
        return $this->belongsTo('App\Property');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
