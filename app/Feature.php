<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{
    protected $fillable = [
        'properties_id', 'name', 'quantity', 'state'
    ];

    public function property(){
        return $this->belongsTo('App\Property');
    }
}
