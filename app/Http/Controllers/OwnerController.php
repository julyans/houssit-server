<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController as BaseController;
use App\Owner;

class OwnerController extends BaseController
{
    public function index(Request $request){
        $owners = Owner::orderBy('id', 'DESC')->paginate(5);
        return [
            'pagination' => [
                'total' => $owners->total(),
                'current_page' => $owners->currentPage(),
                'per_page' => $owners->perPage(),
                'last_page' => $owners->lastPage(),
                'from' => $owners->firstItem(),
                'to' => $owners->lastPage()
            ],
            'owners' => $owners
        ];
    }

    public function indexIdNames(){
        $owners = Owner::select('owners.id','owners.names')->orderBy('id','ASC')->get();
        echo $this->sendResponse($owners, "Datos obtenidos");
    }

    public function indexIdDocument($document_number = ''){
        $owners = Owner::select('owners.id','owners.document','owners.names')
        ->where('document','=',$document_number)->get();
        echo $this->sendResponse($owners, "Datos obtenidos");
    }

    public function store(Request $request){
        if(!empty(!empty($request->type_document) && !empty($request->document) && $request->names) && !empty($request->cellphone) && !empty($request->city) ){
            Owner::create([
                'type_document' => $request->type_document,
                'document' => $request->document,
                'names'=> $request->names,
                'email' => $request->email,
                'phone' => $request->phone,
                'cellphone' => $request->cellphone,
                'city' => $request->city,
                'comments' => $request->comments
            ]);
        }else{
            echo $this->sendError("Error: Los campos {names, cellphone y city} no pueden estar vacios.",$request->toArray());
            exit();
        }
        echo $this->sendResponse($request->toArray(), "Los datos se guardaron correctamente.");
    }

    public function show($id = ''){
        if(!empty($id)){
            $owner = Owner::find($id);
        }else{
            echo $this->sendError("Error: se requiere el valor de {id}.",['id'=>$id]);
            exit();
        }
        
        if(!empty($owner->toArray())){
            echo $this->sendResponse($owner->toArray(), "Datos obtenidos.");
        }else{
            echo $this->sendResponse([], "Ooups! 0 resultados.");
        }
    }

    public function update(Request $request){
        if(!empty($request->names) && !empty($request->cellphone) && !empty($request->city)){
            $owner = Owner::findOrFail($request->id);
            $owner->type_document = $request->type_document;
            $owner->document = $request->document;
            $owner->names = $request->names;
            $owner->email = $request->email;
            $owner->phone = $request->phone;
            $owner->cellphone = $request->cellphone;
            $owner->city = $request->city;
            $owner->comments = $request->comments;

            $owner->save();
        }else{
            echo $this->sendError("Error: Los campos {names, cellphone y city} no pueden estar vacios.",$request->toArray());
            exit();
        }
        echo $this->sendResponse($owner->toArray(), "Los datos se actualizaron correctamente.");        
    }
}