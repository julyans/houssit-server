<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\BaseController as BaseController;
use App\User;

class UserController extends BaseController
{
    public function index(){
        $users = User::orderBy('id', 'ASC')->paginate(5);
        return [
            'pagination' => [
                'total' => $users->total(),
                'current_page' => $users->currentPage(),
                'per_page' => $users->perPage(),
                'last_page' => $users->lastPage(),
                'from' => $users->firstItem(),
                'to' => $users->lastPage()
            ],
            'users' => $users
        ];
    }

    public function store(Request $request){
        if(!empty($request->name) && !empty($request->email) ){
            User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->email)
            ]);
        }else{
            echo $this->sendError("Error: Los campos {nombre y correo electronico} no pueden estar vacios.",$request->toArray());
            exit();
        }
        echo $this->sendResponse($request->toArray(), "Los datos se guardaron correctamente.");
    }

    public function delete($id = ''){
        if(!empty($id)){
            User::where('id','=',$id)->delete();
        }else{
            echo $this->sendError("Error: el id no puede estar vacío.",['id' => $id]);
            exit();
        }
        echo $this->sendResponse(['id' => $id], "Usuario eliminado correctamente.");
    }
}
