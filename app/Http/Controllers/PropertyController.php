<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController as BaseController;
use App\Property;

class PropertyController extends BaseController
{
    public function index(Request $request){
        $properties = Property::join('categories','properties.categories_id','=','categories.id')
        ->join('owners','properties.owners_id','=','owners.id')
        ->select('properties.*','categories.name as category_name','owners.names as owner_name')
        ->orderBy('properties.id','desc')->paginate(5);
        
        return [
            'pagination' => [
                'total' => $properties->total(),
                'current_page' => $properties->currentPage(),
                'per_page' => $properties->perPage(),
                'last_page' => $properties->lastPage(),
                'from' => $properties->firstItem(),
                'to' => $properties->lastPage()
            ],
            'properties' => $properties
        ];
    }

    public function store(Request $request){
        if(!empty($request->categories_id) && !empty($request->owners_id) && !empty($request->name) && !empty($request->business) && !empty($request->address) && !empty($request->city) && !empty($request->neighborhood) && !empty($request->name) && $request->sale_price > 0){
            Property::create([
                'categories_id' => $request->categories_id,
                'owners_id' => $request->owners_id,
                'name'=> $request->name,
                'business' => $request->business,
                'address' => $request->address,
                'city' => $request->city,
                'neighborhood' => $request->neighborhood,
                'construction_area' => $request->construction_area,
                'land_area' => $request->land_area,
                'description' => $request->description,
                'sale_price' => $request->sale_price,
                'property_state' => $request->property_state
            ]);
        }else{
            echo $this->sendError("Error: los campos {name, business, address, city, neighborhood, sale_price, property_state} no pueden estar vacios.",$request->toArray());
            exit();
        }
        echo $this->sendResponse($request->toArray(), "Datos guardados");
    }

    public function show($id = ''){
        if(!empty($id)){
            $property = Property::where('id','=',$id)->where('state','=','1')->get();
        }else{
            echo $this->sendError("Error: se requiere el valor de {id}.",['id'=>$id]);
            exit();
        }

        if(!empty($property->toArray())){
            echo $this->sendResponse($property->toArray(), "Datos obtenidos.");
        }else{
            echo $this->sendResponse([], "Ooups! 0 resultados.");
        }
    }

    public function update(Request $request){
        if(!empty($request->categories_id) && !empty($request->owners_id) && !empty($request->name) && !empty($request->business) && !empty($request->address) && !empty($request->city) && !empty($request->neighborhood) && !empty($request->name) && $request->sale_price > 0){
            $property = Property::findOrFail($request->id);
            $property->categories_id = $request->categories_id;
            $property->owners_id = $request->owners_id;
            $property->name = $request->name;
            $property->business = $request->business;
            $property->address = $request->address;
            $property->city = $request->city;
            $property->neighborhood = $request->neighborhood;
            $property->construction_area = $request->construction_area;
            $property->land_area = $request->land_area;
            $property->description = $request->description;
            $property->sale_price = $request->sale_price;
            $property->property_state = $request->property_state;

            $property->save();
        }else{
            echo $this->sendError("Error: los campos {name, business, address, city, neighborhood, sale_price, property_state} no pueden estar vacios.",$request->toArray());
            exit();
        }
        echo $this->sendResponse($property->toArray(), "Datos actualizados");        
    }

    public function active($id = ''){
        if(!empty($id)){
            $property = Property::findOrFail($id);
            $property->state = 1;
            
            $property->save();
        }else{
            echo $this->sendError("Error: se requiere el valor de {id}.",['id' => $id]);
            exit();
        }
        echo $this->sendResponse($property->toArray(), "Propiedad activada");
    }

    public function desactive($id = ''){
        if(!empty($id)){
            $property = Property::findOrFail($id);
            $property->state = 0;
            
            $property->save();
        }else{
            echo $this->sendError("Error: se requiere el valor de {id}.",['id' => $id]);
            exit();
        }
        echo $this->sendResponse($property->toArray(), "Propiedad desactivada");
    }
}
