<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController as BaseController;
use App\CommonArea;

class CommonAreaController extends BaseController
{
    public function index(){
        $common_areas = CommonArea::orderBy('id', 'DESC')->paginate(5);
        return [
            'pagination' => [
                'total' => $common_areas->total(),
                'current_page' => $common_areas->currentPage(),
                'per_page' => $common_areas->perPage(),
                'last_page' => $common_areas->lastPage(),
                'from' => $common_areas->firstItem(),
                'to' => $common_areas->lastPage()
            ],
            'common_areas' => $common_areas
        ];
    }

    public function store(Request $request){
        if(!empty($request->properties_id) && !empty($request->name)){
            CommonArea::create([
                'properties_id'=> $request->properties_id,
                'name'=> $request->name
            ]);
        }else{
            echo $this->sendError("Error: el campo {nombre} no puede estar vacio.",$request->toArray());
            exit();
        }
        echo $this->sendResponse($request->toArray(), "Datos guardados");
    }

    public function show($id = ''){
        if(!empty($id)){
            $common_area = CommonArea::where('id','=',$id)->where('state','=','1')->get();
        }else{
            echo $this->sendError("Error: se requiere el valor de {id}.",['id'=>$id]);
            exit();
        }
        
        if(!empty($common_area->toArray())){
            echo $this->sendResponse($common_area->toArray(), "Datos obtenidos.");
        }else{
            echo $this->sendResponse([], "Ooups! 0 resultados.");
        }
    }

    public function showCommonAreas($properties_id = ''){
        if(!empty($properties_id)){
            $common_areas = CommonArea::where('properties_id','=',$properties_id)->where('state','=','1')->get();
        }else{
            echo $this->sendError("Error: se requiere el valor de {properties_id}.",['properties_id'=>$properties_id]);
            exit();
        }
        
        if(!empty($common_areas->toArray())){
            echo $this->sendResponse($common_areas->toArray(), "Datos obtenidos.");
        }else{
            echo $this->sendResponse([], "Ooups! 0 resultados.");
        }
    }

    public function update(Request $request){
        if(!empty($request->properties_id) && !empty($request->name)){
            $common_area = CommonArea::findOrFail($request->id);
            $common_area->name = $request->name;
            
            $common_area->save();
        }else{
            echo $this->sendError("Error: el campo {nombre} no puede estar vacio.",$request->toArray());
            exit();
        }
        echo $this->sendResponse($common_area->toArray(), "Datos actualizados");        
    }

    public function active($id = ''){
        if(!empty($id)){
            $common_area = CommonArea::findOrFail($id);
            $common_area->state = 1;
            
            $common_area->save();
        }else{
            echo $this->sendError("Error: se requiere el valor de {id}.",['id' => $id]);
            exit();
        }
        echo $this->sendResponse($common_area->toArray(), "Área común activada");
    }

    public function desactive($id = ''){
        if(!empty($id)){
            $common_area = CommonArea::findOrFail($id);
            $common_area->state = 0;
            
            $common_area->save();
        }else{
            echo $this->sendError("Error: se requiere el valor de {id}.",['id' => $id]);
            exit();
        }
        echo $this->sendResponse($common_area->toArray(), "Área común desactivada");
    }
}