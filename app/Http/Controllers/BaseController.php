<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BaseController extends Controller
{
    public function sendResponse($result,$message){
        $response = [
            'success' => true,
            'data' => $result,
            'message' => $message
        ];
        return json_encode($response, JSON_FORCE_OBJECT);
    }

    public function sendError($error, $errorMessages = []){
        $response = [
            'success' => false,
            'message' => $error
        ];
        if(!empty($errorMessages)){
            $response['data'] = $errorMessages;
        }
        return json_encode($response, JSON_FORCE_OBJECT);
    }

    public function getUserId(){
        if (Auth::check()) {
            $id = Auth::id();
            return json_encode(['success' => true, 'id' => $id],JSON_FORCE_OBJECT);
            exit();
        }
        return json_encode(['success' => false, 'message' => 'error, usuario no autenticado'],JSON_FORCE_OBJECT);
    }
}
