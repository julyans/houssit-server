<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController as BaseController;
use App\Http\Controllers\PhotoController;
use App\Feature;
use Image;
use App\Photo;

class FeatureController extends BaseController
{
    public function index(){
        $features = Feature::orderBy('id', 'DESC')->paginate(5);
        return [
            'pagination' => [
                'total' => $features->total(),
                'current_page' => $features->currentPage(),
                'per_page' => $features->perPage(),
                'last_page' => $features->lastPage(),
                'from' => $features->firstItem(),
                'to' => $features->lastPage()
            ],
            'features' => $features
        ];
    }

    public function loadPhoto(Request $request){
        // This validate the params of the request
        $this->validate($request, [
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
        
        // Here we define the routes for the saving of the resized images
        $destinationPath_photoLarge = public_path('/images/properties/photo_large');
        $destinationPath_photoMedium = public_path('/images/properties/photo_medium');
        $destinationPath_photoSmall = public_path('/images/properties/photo_small');

        $image = $request->file('image');  
        $input['image_name'] = time().'.'.$image->extension();

        $img = Image::make($image->path());

        // resize for photo_large version
        // ============================================
        // This insert a watermark in the photography
        $img->insert(public_path('/images/avatars/logotypes/logo_thumbnail.png'), 'top-right', 10, 10);
        
        // This resize the photo to especificied size
        $img->resize(2048, 1536, function ($constraint){
            $constraint->aspectRatio();
        })->save($destinationPath_photoLarge.'/'.$input['image_name']);
        
        // resize for photo_medium version
        // ============================================
        
        // This resize the photo to especificied size
        $img->resize(1024, 768, function ($constraint){
            $constraint->aspectRatio();
        })->save($destinationPath_photoMedium.'/'.$input['image_name']);

        // resize for photo_small version
        // ============================================
        
        // This resize the photo to especificied size
        $img->resize(320, 280, function ($constraint){
            $constraint->aspectRatio();
        })->save($destinationPath_photoSmall.'/'.$input['image_name']);

        // Save image with the original version
        $destinationPath_photoOriginal = public_path('/images/properties');
        $image->move($destinationPath_photoOriginal, $input['image_name']);
        
        return [
            'success' => true, 'image_name' => $input['image_name']
        ];
    }

    public function loadPhotoForFeature(Request $request){
        if(!empty($request->properties_id) && !empty($request->users_id) && !empty($request->image)){
            $data = $this->loadPhoto($request);
            if($data['success']){
                Photo::create([
                    'properties_id'=> $request->properties_id,
                    'users_id'=> $request->users_id,
                    'photo_name' => $data['image_name'],
                    'is_feature' => $request->is_feature
                ]);
            }else{
                echo $this->sendError("Error: ocurrió un error al crear la imagen en el servidor.",$request->toArray());
                exit();
            }
        }
    }

    public function store(Request $request){
        if(!empty($request->properties_id) && !empty($request->name) && $request->quantity > 0){
            Feature::create([
                'properties_id'=> $request->properties_id,
                'name'=> $request->name,
                'quantity' => $request->quantity
            ]);
            $this->loadPhotoForFeature($request);
        }else{
            echo $this->sendError("Error: el campo {nombre y cantidad} no pueden estar vacios.",$request->toArray());
            exit();
        }
        echo $this->sendResponse($request->toArray(), "Datos guardados");
    }

    public function show($id = ''){
        if(!empty($id)){
            $feature = Feature::where('id','=',$id)->where('state','=','1')->get();
        }else{
            echo $this->sendError("Error: se requiere el valor de {id}.",['id'=>$id]);
            exit();
        }
        
        if(!empty($feature->toArray())){
            echo $this->sendResponse($feature->toArray(), "Datos obtenidos.");
        }else{
            echo $this->sendResponse([], "Ooups! 0 resultados.");
        }
    }

    public function showFeatures($properties_id = ''){
        if(!empty($properties_id)){
            $features = Feature::where('properties_id','=',$properties_id)->where('state','=','1')->get();
        }else{
            echo $this->sendError("Error: se requiere el valor de {properties_id}.",['properties_id'=>$properties_id]);
            exit();
        }
        
        if(!empty($features->toArray())){
            echo $this->sendResponse($features->toArray(), "Datos obtenidos.");
        }else{
            echo $this->sendResponse([], "Ooups! 0 resultados.");
        }
    }

    public function update(Request $request){
        if(!empty($request->properties_id) && !empty($request->name) && $request->quantity > 0){
            $feature = Feature::findOrFail($request->id);
            $feature->name = $request->name;
            $feature->quantity = $request->quantity;
            
            $feature->save();
        }else{
            echo $this->sendError("Error: los campos {nombre y cantidad} no pueden estar vacios.",$request->toArray());
            exit();
        }
        echo $this->sendResponse($feature->toArray(), "Datos actualizados");        
    }

    public function active($id = ''){
        if(!empty($id)){
            $feature = Feature::findOrFail($id);
            $feature->state = 1;
            
            $feature->save();
        }else{
            echo $this->sendError("Error: se requiere el valor de {id}.",['id' => $id]);
            exit();
        }
        echo $this->sendResponse($feature->toArray(), "Caracteristica activada");
    }

    public function desactive($id = ''){
        if(!empty($id)){
            $feature = Feature::findOrFail($id);
            $feature->state = 0;
            
            $feature->save();
        }else{
            echo $this->sendError("Error: se requiere el valor de {id}.",['id' => $id]);
            exit();
        }
        echo $this->sendResponse($feature->toArray(), "Caracteristica desactivada");
    }
}