<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController as BaseController;
use App\Photo;
use Image;
use File;

class PhotoController extends BaseController
{
    public function index(){
        $photos = Photo::where('state','=','1')->orderBy('id','asc')->paginate(5);
        return [
            'pagination' => [
                'total' => $photos->total(),
                'current_page' => $photos->currentPage(),
                'per_page' => $photos->perPage(),
                'last_page' => $photos->lastPage(),
                'from' => $photos->firstItem(),
                'to' => $photos->lastPage()
            ],
            'photos' => $photos
        ];
    }

    public function loadPhoto(Request $request){
        // This validate the params of the request
        $this->validate($request, [
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
        
        // Here we define the routes for the saving of the resized images
        $destinationPath_photoLarge = public_path('/images/properties/photo_large');
        $destinationPath_photoMedium = public_path('/images/properties/photo_medium');
        $destinationPath_photoSmall = public_path('/images/properties/photo_small');

        $image = $request->file('image');  
        $input['image_name'] = time().'.'.$image->extension();

        $img = Image::make($image->path());

        // resize for photo_large version
        // ============================================
        // This insert a watermark in the photography
        $img->insert(public_path('/images/avatars/logotypes/logo_thumbnail.png'), 'top-right', 10, 10);
        
        // This resize the photo to especificied size
        $img->resize(2048, 1536, function ($constraint){
            $constraint->aspectRatio();
        })->save($destinationPath_photoLarge.'/'.$input['image_name']);
        
        // resize for photo_medium version
        // ============================================
        
        // This resize the photo to especificied size
        $img->resize(1024, 768, function ($constraint){
            $constraint->aspectRatio();
        })->save($destinationPath_photoMedium.'/'.$input['image_name']);

        // resize for photo_small version
        // ============================================
        
        // This resize the photo to especificied size
        $img->resize(320, 280, function ($constraint){
            $constraint->aspectRatio();
        })->save($destinationPath_photoSmall.'/'.$input['image_name']);

        // Save image with the original version
        $destinationPath_photoOriginal = public_path('/images/properties');
        $image->move($destinationPath_photoOriginal, $input['image_name']);
        
        return [
            'success' => true, 'image_name' => $input['image_name']
        ];
    }

    public function store(Request $request){     
        if(!empty($request->properties_id) && !empty($request->users_id) && !empty($request->image)){
            if(!$this->if_exits($request->properties_id)['success']){
                $data = $this->loadPhoto($request);
                if($data['success']){
                    Photo::create([
                        'properties_id'=> $request->properties_id,
                        'users_id'=> $request->users_id,
                        'photo_name' => $data['image_name'],
                        'is_feature' => $request->is_feature
                    ]);
                }else{
                    echo $this->sendError("Error: ocurrió un error al crear la imagen en el servidor.",$request->toArray());
                    exit();
                }
            }else{
                echo $this->sendError("Error: no se puede crear mas de una fotografía principal.",$request->toArray());
                exit();
            }
        }else{
            echo $this->sendError("Error: los datos de {properties_id, users_id e image} son requeridos.",$request->toArray());
            exit();
        }
        echo $this->sendResponse($request->toArray(), "Imagen guardada.");
    }

    public function if_exits($properties_id = ''){
        $value = false;
        $id = '';
        $photo_name = '';
        if(!empty($properties_id)){
            $photo = Photo::where('properties_id','=',$properties_id)
            ->where('is_feature','=','1')->get();
            $value = count($photo) > 0 ? true : false;
            if($value){
                foreach($photo as $p){
                    $id = $p['id'];
                    $photo_name = $p['photo_name'];
                }
            }
        }
        return $value ? ['photo_id' => $id, 'photo_name' => $photo_name, 'success' => $value] : ['success' => false];
    }

    public function update(Request $request){
        if(!empty($request->properties_id) && !empty($request->image)){
            $if_exits_array = $this->if_exits($request->properties_id);
            if($if_exits_array['success']){
                $response = $this->delete($if_exits_array['photo_name']);
                if($response['status']){
                    $data = $this->loadPhoto($request);
                    if($data['success']){
                        $photo = Photo::findOrFail($if_exits_array['photo_id']);
                        $photo->photo_name = $data['image_name'];
                        
                        $photo->save();
                        echo $this->sendResponse($photo->toArray(), "Foto actualizada.");
                        exit();
                    }
                }
            }else{
                echo $this->sendError("Error: no existe fotografía de la propiedad {id}.",['id' => $if_exits_array['photo_id']]);
                exit();
            }
        }else{
            echo $this->sendError("Error: se requiere el valor de {id e image}.",['data' => $request]);
            exit();
        }
    }

    public function delete($image_name = ''){
        $status = false;
        $image_path = public_path("/images/properties"."/".$image_name);
        if(File::exists($image_path)) {
            File::delete($image_path);
            $status = true;
            $image_path_large = public_path("/images/properties/photo_large"."/".$image_name);
            if(File::exists($image_path_large)){
                File::delete($image_path_large);
            }
            
            $image_path_medium = public_path("/images/properties/photo_medium"."/".$image_name);
            if(File::exists($image_path_medium)){
                File::delete($image_path_medium);
            }

            $image_path_small = public_path("/images/properties/photo_small"."/".$image_name);
            if(File::exists($image_path_small)){
                File::delete($image_path_small);
            }
        }
        return ['status' => $status];
    }

    public function show($id = ''){
        if(!empty($id)){
            $photo = Photo::find($id);
        }else{
            echo $this->sendError("Error: el {id} es requerido.",$photo->toArray());
            exit();
        }
        echo $this->sendResponse($photo->toArray(), "Foto obtenida.");
    }

    public function active($id = ''){
        if(!empty($id)){
            $photo = Photo::findOrFail($id);
            $photo->state = 1;
            
            $photo->save();
        }else{
            echo $this->sendError("Error: se requiere el valor de {id}.",['id' => $id]);
            exit();
        }
        echo $this->sendResponse($photo->toArray(), "Foto activada");
    }

    public function desactive($id = ''){
        if(!empty($id)){
            $photo = Photo::findOrFail($id);
            $photo->state = 0;
            
            $photo->save();
        }else{
            echo $this->sendError("Error: se requiere el valor de {id}.",['id' => $id]);
            exit();
        }
        echo $this->sendResponse($photo->toArray(), "Foto desactivada");
    }
}
