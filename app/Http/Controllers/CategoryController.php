<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController as BaseController;
use App\Category;

class CategoryController extends BaseController {
    public function index(Request $request){
        $categories = Category::orderBy('id', 'DESC')->paginate(5);
        return [
            'pagination' => [
                'total' => $categories->total(),
                'current_page' => $categories->currentPage(),
                'per_page' => $categories->perPage(),
                'last_page' => $categories->lastPage(),
                'from' => $categories->firstItem(),
                'to' => $categories->lastPage()
            ],
            'categories' => $categories
        ];
    }

    public function indexIdName(){
        $categories = Category::select('categories.id','categories.name')
        ->where('state','=','1')
        ->orderBy('id','ASC')->get();
        echo $this->sendResponse($categories, "Datos obtenidos");
    }

    public function store(Request $request){
        if(!empty($request->name)){
            Category::create([
                'name'=> $request->name,
                'description' => $request->description
            ]);
        }else{
            echo $this->sendError("Error: el campo {nombre} no puede estar vacio.",$request->toArray());
            exit();
        }
        echo $this->sendResponse($request->toArray(), "Datos guardados");
    }

    public function show($id = ''){
        if(!empty($id)){
            $category = Category::where('id','=',$id)->where('state','=','1')->get();
        }else{
            echo $this->sendError("Error: se requiere el valor de {id}.",['id'=>$id]);
            exit();
        }
        
        if(!empty($category->toArray())){
            echo $this->sendResponse($category->toArray(), "Datos obtenidos.");
        }else{
            echo $this->sendResponse([], "Ooups! 0 resultados.");
        }
    }

    public function update(Request $request){
        if(!empty($request->name)){
            $category = Category::findOrFail($request->id);
            $category->name = $request->name;
            $category->description = $request->description;
            
            $category->save();
        }else{
            echo $this->sendError("Error: el campo {nombre} no puede estar vacio.",$request->toArray());
            exit();
        }
        echo $this->sendResponse($category->toArray(), "Datos actualizados");        
    }

    public function active($id = ''){
        if(!empty($id)){
            $category = Category::findOrFail($id);
            $category->state = 1;
            
            $category->save();
        }else{
            echo $this->sendError("Error: se requiere el valor de {id}.",['id' => $id]);
            exit();
        }
        echo $this->sendResponse($category->toArray(), "Categoría activada");
    }

    public function desactive($id = ''){
        if(!empty($id)){
            $category = Category::findOrFail($id);
            $category->state = 0;
            
            $category->save();
        }else{
            echo $this->sendError("Error: se requiere el valor de {id}.",['id' => $id]);
            exit();
        }
        echo $this->sendResponse($category->toArray(), "Categoría desactivada");
    }
}
