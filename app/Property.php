<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    protected $fillable = [
        'categories_id','owners_id','name','business','address','city','neighborhood','construction_area','land_area','description','sale_price','property_state','state'
    ];

    public function category(){
        return $this->belongsTo('App\Category');
    }

    public function owner(){
        return $this->belongsTo('App\Owner');
    }

    public function feature(){
        return $this->hasMany('App\Feature');
    }

    public function common_area(){
        return $this->hasMany('App\CommonArea');
    }

    public function photo(){
        return $this->hasMany('App\Photo');
    }
}
