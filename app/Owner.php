<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Owner extends Model
{
    protected $fillable = [
        'type_document','document','names','email','phone','cellphone','city','comments'
    ];
}
