<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommonArea extends Model
{
    protected $fillable = [
        'properties_id', 'name', 'state'
    ];

    public function property(){
        return $this->belongsTo('App\Property');
    }
}
