<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('categories_id')->unsigned();
            $table->bigInteger('owners_id')->unsigned();
            $table->string('name',50);
            $table->string('business',10);
            $table->string('address',255);
            $table->string('city',255);
            $table->string('neighborhood',255);
            $table->decimal('construction_area',11,2)->nullable();
            $table->decimal('land_area',11,2)->nullable();
            $table->string('description',255)->nullable();
            $table->decimal('sale_price',15,2);
            $table->string('property_state',100)->nullable();
            $table->boolean('state')->default(1);
            $table->timestamps();

            $table->foreign('categories_id')->references('id')->on('categories');
            $table->foreign('owners_id')->references('id')->on('owners');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
