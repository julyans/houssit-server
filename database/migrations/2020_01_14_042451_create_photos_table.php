<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('photos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('properties_id')->unsigned();
            $table->bigInteger('users_id')->unsigned();
            $table->string('photo_name',255);
            $table->boolean('is_feature')->default(0);
            $table->boolean('state')->default(1);
            $table->timestamps();

            $table->foreign('properties_id')->references('id')->on('properties');
            $table->foreign('users_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('photos');
    }
}
