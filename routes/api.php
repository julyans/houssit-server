<?php

use Illuminate\Http\Request;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(["middleware" => "apikey.validate"], function(){
    /**=========================== */
    // Routes protected for apikey
    /**=========================== */

    // BaseController
    Route::get('/auth/getIdAuthUser','BaseController@getUserId');

    // Categories
    Route::get('/category/index','CategoryController@index');
    Route::get('/category/indexIdName','CategoryController@indexIdName');
    Route::post('/category/store','CategoryController@store');
    Route::post('/category/update','CategoryController@update');
    Route::get('/category/show/{id?}','CategoryController@show');
    Route::get('/category/active/{id?}','CategoryController@active');
    Route::get('/category/desactive/{id?}','CategoryController@desactive');

    // Owners
    Route::get('/owner/index','OwnerController@index');
    Route::get('/owner/indexIdNames','OwnerController@indexIdNames');
    Route::get('/owner/indexIdDocument/{document_number?}','OwnerController@indexIdDocument');
    Route::post('/owner/store','OwnerController@store');
    Route::post('/owner/update','OwnerController@update');
    Route::get('/owner/show/{id?}','OwnerController@show');

    // Properties
    Route::get('/property/index','PropertyController@index');
    Route::post('/property/store','PropertyController@store');
    Route::post('/property/update','PropertyController@update');
    Route::get('/property/show/{id?}','PropertyController@show');
    Route::get('/property/active/{id?}','PropertyController@active');
    Route::get('/property/desactive/{id?}','PropertyController@desactive');

    // Features
    Route::get('/feature/index','FeatureController@index');
    Route::post('/feature/store','FeatureController@store');
    Route::post('/feature/update','FeatureController@update');
    Route::get('/feature/show/{id?}','FeatureController@show');
    Route::get('/feature/showFeatures/{properties_id?}','FeatureController@showFeatures');
    Route::get('/feature/active/{id?}','FeatureController@active');
    Route::get('/feature/desactive/{id?}','FeatureController@desactive');

    // CommonAreas
    Route::get('/common_area/index','CommonAreaController@index');
    Route::post('/common_area/store','CommonAreaController@store');
    Route::post('/common_area/update','CommonAreaController@update');
    Route::get('/common_area/show/{id?}','CommonAreaController@show');
    Route::get('/common_area/showCommonAreas/{properties_id?}','CommonAreaController@showCommonAreas');
    Route::get('/common_area/active/{id?}','CommonAreaController@active');
    Route::get('/common_area/desactive/{id?}','CommonAreaController@desactive');

    // Photos
    Route::get('/photo/index','PhotoController@index');
    Route::post('/photo/store', 'PhotoController@store');
    Route::post('/photo/update','PhotoController@update');
    Route::get('/photo/show/{id?}','PhotoController@show');
    Route::get('/photo/active/{id?}','PhotoController@active');
    Route::get('/photo/desactive/{id?}','PhotoController@desactive');
    Route::get('/photo/if_exits/{properties_id?}', 'PhotoController@if_exits');

    Route::get('/photo/delete/{image_name?}','PhotoController@delete'); // To Do

    // Users
    Route::get('/user/index','UserController@index');
    Route::post('/user/store', 'UserController@store');
    Route::get('/user/delete/{id?}','UserController@delete');
});